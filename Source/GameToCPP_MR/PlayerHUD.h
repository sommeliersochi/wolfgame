// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseUserWidget.h"
#include "PlayerHUD.generated.h"

/**
 * 
 */
UCLASS()
class GAMETOCPP_MR_API UPlayerHUD : public UBaseUserWidget
{
	GENERATED_BODY()
protected:
	UFUNCTION(BlueprintCallable,BlueprintPure)
FText GetHealth();

		UFUNCTION(BlueprintCallable, BlueprintPure)
FText GetCountMeat();

	UFUNCTION(BlueprintCallable,BlueprintPure)
FText GetTimerText();
	
	UFUNCTION(BlueprintCallable,BlueprintPure)
bool IsLiveCharacter();
	
	UFUNCTION(BlueprintCallable)
FText FloatToText (float Value, int MinimalIntegralDegits = 2);
};
