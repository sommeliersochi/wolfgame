// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHUD.h"

#include "Character/BaseCharacter.h"
#include "Kismet/KismetTextLibrary.h"

FText UPlayerHUD::GetHealth()
{
	ABaseCharacter* Character = Cast<ABaseCharacter>(GetPawn());
	if(Character!=nullptr) return FloatToText(Character->GetHealth());
	return FText();
}

FText UPlayerHUD::GetCountMeat()
{
	ABaseCharacter* Character = Cast<ABaseCharacter>(GetPawn());
	if(Character!=nullptr) return FloatToText(Character->GetCountFood());
	return FText();
}
	


FText UPlayerHUD::GetTimerText()
{
	float SecondsValue = GetWorld()-> GetTimeSeconds();
	float SantisecondsValue = FMath::RoundToInt(SecondsValue * 100) % 100;
	FTimespan Timespan = FTimespan::FromSeconds(SecondsValue);

	FText Hours = FloatToText(Timespan.GetHours());
	FText Minutes = FloatToText(Timespan.GetMinutes());
	FText Seconds = FloatToText(Timespan.GetSeconds());
	FText Santiseconds = FloatToText(SantisecondsValue);
	
	FString StringFormat ("{Hours}:{Minutes}:{Seconds}.{Santiseconds}");
	FTextFormat Format = FTextFormat::FromString(StringFormat);

	return FText::Format(Format, Hours,Minutes,Seconds,Santiseconds);
}

bool UPlayerHUD::IsLiveCharacter()
{
	ABaseCharacter* Character = Cast<ABaseCharacter>(GetPawn());
	if(Character!=nullptr) return !Character -> IsDead();
	return false;
	
}

FText UPlayerHUD::FloatToText(float Value, int MinimalIntegralDegits)
{
	return UKismetTextLibrary::Conv_FloatToText(Value, ERoundingMode::FromZero, false, false,2,3,0,0 );
	
}
