// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MainHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeath);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMETOCPP_MR_API UMainHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMainHealthComponent();
	
	UPROPERTY(BlueprintReadWrite,BlueprintAssignable,BlueprintCallable)
	FDeath fDeath;
	
	void AddHealth(float Add);
	void Damage(float Damage);
	
	float GetHealth();
	bool IsDead();
	//void Recovery (float MaxHealth);
	
private:
	float Health;
	bool bIsDead;
	UPROPERTY(EditDefaultsOnly)
	float MaxHealth;		
};
