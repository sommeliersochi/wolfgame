// Fill out your copyright notice in the Description page of Project Settings.


#include "MainHealthComponent.h"


// Sets default values for this component's properties
UMainHealthComponent::UMainHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	bIsDead = false;
	MaxHealth = 100;
	Health = MaxHealth;

	// ...
}

void UMainHealthComponent::AddHealth(float Add)
{
	float TempH = Health + Add;
	Health = FMath::Min(TempH, MaxHealth);
	
}

void UMainHealthComponent::Damage(float Damage)
{
	float TempD = Health - Damage;
	Health = FMath::Max(TempD, 0.0f);
	if(Health == 0 && !bIsDead)
	{
		bIsDead = true;
		fDeath.Broadcast();
	}
	
}

float UMainHealthComponent::GetHealth()
{
	return Health;
}

bool UMainHealthComponent::IsDead()
{
	return bIsDead;
}





