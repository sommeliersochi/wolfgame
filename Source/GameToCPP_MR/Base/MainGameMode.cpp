// Copyright Epic Games, Inc. All Rights Reserved.


#include "MainGameMode.h"

#include "GameToCPP_MR/MainHUD.h"


AMainGameMode::AMainGameMode() : Super()
{
	const ConstructorHelpers::FClassFinder<APawn> PlayerClass (TEXT("/Game/Blueprints/BP_PlayerCharacter"));
	if(PlayerClass.Class!=nullptr)
		{
		DefaultPawnClass = PlayerClass.Class;
	}
	HUDClass = AMainHUD::StaticClass();
}


