// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Food.h"

#include "EatAIController.generated.h"

/**
 * 
 */
UCLASS()
class GAMETOCPP_MR_API AEatAIController : public AAIController
{
	GENERATED_BODY()
public:
	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;
protected:

	
	virtual void OnPossess(APawn* InPawn) override;
	void CheckFood();
	void Eat();
	void StartEatWithDelay();
	//void FindGoodEat(AActor* OtherActor) const;

private:
	
	AActor* NearestFood;	
	AActor* GetNearestFood() const;
	AActor* BestChois;


	APawn* Pawn;
};
