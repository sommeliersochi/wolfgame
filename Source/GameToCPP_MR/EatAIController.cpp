// Fill out your copyright notice in the Description page of Project Settings.


#include "EatAIController.h"
#include "Kismet/GameplayStatics.h"
#include "Food.h"



void AEatAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	Pawn = InPawn;
	Eat();
	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AEatAIController::CheckFood, 1, true, 0);
}



void AEatAIController::CheckFood()
{
	AActor* NewNearestFood = GetNearestFood();
	if (NewNearestFood != NearestFood)
	{
		NearestFood = NewNearestFood;
		StopMovement();
	}
}

void AEatAIController::Eat()
{
	if (NearestFood == nullptr)
	{
		StartEatWithDelay();
		return;
	}
	const FAIMoveRequest MoveRequest(NearestFood);
	MoveTo(MoveRequest);
}

void AEatAIController::StartEatWithDelay()
{
	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AEatAIController::Eat, 0.1, false, 0);
}

/*
 *void AEatAIController::FindGoodEat(AActor* OtherActor) const
{
	AFood* Food = Cast<AFood>(OtherActor);
	if(Food->GetAddHealth() > Food->GetAddHealth()) return Food
}
*/

void AEatAIController::OnMoveCompleted(FAIRequestID RequestID,
	const FPathFollowingResult& Result)
{
	StartEatWithDelay();
}




AActor* AEatAIController::GetNearestFood() const
{
	TArray<AActor*> OutActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(),
		AFood::StaticClass(),
		OutActors);
	
	if (OutActors.Num() > 0)
	{
		AActor* LNearestFood = OutActors.HeapTop();
		for (AActor* Actor : OutActors)
		{
			if (Actor->GetDistanceTo(Pawn) < LNearestFood->GetDistanceTo(Pawn))
				LNearestFood = Actor;

		}
		return LNearestFood;


	}
	return nullptr;
}



