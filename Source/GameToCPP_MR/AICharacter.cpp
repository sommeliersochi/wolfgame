// Fill out your copyright notice in the Description page of Project Settings.


#include "AICharacter.h"

#include "EatAIController.h"

AAICharacter::AAICharacter() : Super()
{
	AIControllerClass = AEatAIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}
