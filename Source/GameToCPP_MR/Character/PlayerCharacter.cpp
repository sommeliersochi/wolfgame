// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"


APlayerCharacter::APlayerCharacter():Super()
{
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 500.0f;
	CameraBoom->bUsePawnControlRotation = true;
	FollouCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollouCamera"));
	FollouCamera->SetupAttachment(CameraBoom);
	FollouCamera->bUsePawnControlRotation = false;
	
	
	
}

void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent-> BindAxis("MoveForvard", this, &APlayerCharacter::MoveFB);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveLR);
	PlayerInputComponent->BindAxis("LookLeftRight", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUpDown", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAction("Jump", IE_Pressed,this, &APlayerCharacter::Jump);
	
}

void APlayerCharacter::MoveFB(float Value)
{
	Move(EAxis::X, Value);
}

void APlayerCharacter::MoveLR(float Value)
{	
    Move(EAxis::Y, Value);
}


void APlayerCharacter::Move(EAxis::Type axis, float Value)
{
	if((Controller!= nullptr)&&(Value!=0))
	{
		FRotator Rotator = Controller->GetControlRotation();
		FRotator YawRotator(0, Rotator.Yaw, 0);
		FVector Direction =   FRotationMatrix (YawRotator).GetUnitAxis(axis);
		AddMovementInput(Direction,Value);
	}
}
