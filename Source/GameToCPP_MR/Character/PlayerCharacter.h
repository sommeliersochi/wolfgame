// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

#include "PlayerCharacter.generated.h"

/**
 * 
 */
UCLASS()
class GAMETOCPP_MR_API APlayerCharacter : public ABaseCharacter
{
	GENERATED_BODY()
	public:
	APlayerCharacter();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	private:
	UPROPERTY(VisibleAnywhere)
	USpringArmComponent* CameraBoom;
	UPROPERTY(VisibleAnywhere)
	UCameraComponent* FollouCamera;

	void MoveFB(float Value);
	void MoveLR(float Value);


	
	

	void Move(EAxis::Type axis, float Value);
	
	
};
