// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"

#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"


// Sets default values
ABaseCharacter::ABaseCharacter() : Super()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	DamagePerSecond = 1;
	TimerRate = 0.1;
	CountFood = 0;
	GetMesh()->SetCollisionProfileName("SkeletalMesh");
	
	GetCharacterMovement()->bOrientRotationToMovement = true;
	HealthComponent = CreateDefaultSubobject<UMainHealthComponent>(TEXT("HealthComponent"));
	HealthComponent->fDeath.AddDynamic(this,&ABaseCharacter::OnDead);

}

FText ABaseCharacter::GetName()
{
	return FText::FromString(UKismetSystemLibrary::GetDisplayName(this));
	
}

float ABaseCharacter::GetCountFood()
{
	return CountFood;
}

float ABaseCharacter::GetHealth()
{
	if(HealthComponent!=nullptr) return HealthComponent->GetHealth();
	return 0;
}

bool ABaseCharacter::IsDead()
{
	return HealthComponent->IsDead();
}

void ABaseCharacter::AddHealth(float AddHealth)
{
	if(HealthComponent!=nullptr) HealthComponent->AddHealth(AddHealth);
}

void ABaseCharacter::EatFood()
{
	CountFood++;
	if(Sound!=nullptr)UGameplayStatics::SpawnSoundAtLocation(this,Sound, GetActorLocation());
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle DamageTimer;
	GetWorldTimerManager().SetTimer(DamageTimer, this, &ABaseCharacter::Damage, TimerRate, true, 0 );

}
void ABaseCharacter::Damage()
{
	if(HealthComponent!=nullptr) HealthComponent->Damage(TimerRate*DamagePerSecond);
	//GEngine->AddOnScreenDebugMessage(-1,1,FColor::Orange,FString::FromInt(GetHealth()));
	
	
}

void ABaseCharacter::OnDead()
{
	if(AnimMontageDeath!=nullptr) PlayAnimMontage(AnimMontageDeath);
	UnPossessed();
	GetMesh()->SetSimulatePhysics(true);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
}









