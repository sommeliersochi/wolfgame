// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameToCPP_MR/Component/MainHealthComponent.h"

#include "BaseCharacter.generated.h"

UCLASS()
class GAMETOCPP_MR_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();
	FText GetName();
	float GetCountFood();
	float GetHealth();
	bool IsDead();
	void AddHealth(float AddHealth);
	void EatFood();
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	

private:
	int CountFood;
	float TimerRate;
	UPROPERTY(VisibleAnywhere)
	UMainHealthComponent* HealthComponent;
	UPROPERTY(EditDefaultsOnly, Category="Settings")
	float DamagePerSecond;
	UPROPERTY(EditDefaultsOnly, Category="Settings")
	USoundBase* Sound;
	UPROPERTY(EditDefaultsOnly, Category="Settings")
	UAnimMontage*AnimMontageDeath;
	UPROPERTY(EditDefaultsOnly, Category="Settings")
	UAnimMontage*AnimMontageJump;
	
	void Damage();
	UFUNCTION(BlueprintCallable)
	void OnDead();
	

};
