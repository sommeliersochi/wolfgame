// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"

#include "Character/BaseCharacter.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	AddedHealth = 5;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	Mesh -> SetSimulatePhysics(true);

	Mesh -> SetCollisionProfileName(TEXT("Food"));

	RootComponent = Mesh;
	Mesh -> OnComponentBeginOverlap.AddDynamic(this, &AFood::OnPickup);

	
	

}

int AFood::GetAddHealth() const
{
	return AddedHealth;
}

void AFood::OnPickup(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                     int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ABaseCharacter* Character = Cast<ABaseCharacter>(OtherActor);
	Character-> EatFood();
	Character->AddHealth(AddedHealth);
	Destroy();
}


