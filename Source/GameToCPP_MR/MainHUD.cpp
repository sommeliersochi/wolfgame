// Fill out your copyright notice in the Description page of Project Settings.


#include "MainHUD.h"

#include "Blueprint/UserWidget.h"

AMainHUD::AMainHUD():Super()
{
	ConstructorHelpers::FClassFinder<UUserWidget>Wbphud(TEXT("/Game/UI/W_HUD"));
	HUDClass = Wbphud.Class;
}

void AMainHUD::BeginPlay()
{
	Super::BeginPlay();
	if(HUDClass != nullptr)
	{
		UUserWidget* Widget = CreateWidget<UUserWidget>(GetWorld(), HUDClass);
		if(Widget)
			Widget -> AddToViewport();
	}
	
}
